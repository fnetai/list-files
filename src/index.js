import { globSync } from 'glob';
import path from "node:path";

// @ai: Parameters are same as 'glob' nmp package. So we can use the same documentation.
export default ({
  dir,
  pattern = "*",
  nodir = false,
  dot = true,
  ignore=[],
  absolute = false,
  matchBase = false,
  follow = false
}) => {

  // set default dir to current working directory if not provided
  dir = dir || process.cwd();
  dir = path.resolve(dir);

  // Converts a single pattern string into an array.
  if (pattern && !Array.isArray(pattern)) pattern = [pattern];
  else pattern = pattern || ["*"];

  // Converts a single ignore pattern string into an array.
  if (ignore && !Array.isArray(ignore)) ignore = [ignore];
  else ignore = ignore || [];

  const options = { cwd: dir, nodir, dot, ignore, absolute, matchBase, follow };

  const result = globSync(pattern, options);

  return result;
};