# @fnet/list-files

This project is a simple utility designed to list files matching specified patterns within a directory. It is primarily used to retrieve an array of file paths based on the defined criteria, supporting versatile pattern matching with additional options to customize the search. It leverages the functionality provided by the 'glob' library to achieve this.

## How It Works

The utility operates by accepting a directory and a pattern (or patterns) to match files within the specified directory. It resolves the directory path, and then uses these inputs along with several optional parameters to generate a list of file paths that align with the given criteria.

## Key Features

- **Pattern Matching**: Search for files using simple pattern syntax.
- **Directory Specification**: Specify the directory to search within; defaults to the current working directory if not provided.
- **Inclusion of Dot Files**: Option to include or exclude dot files (hidden files).
- **Exclude Directories**: Decide whether to include directories in the results.
- **Ignore Patterns**: Set patterns to ignore certain files or directories.
- **Absolute Paths**: Choose whether the returned paths are absolute.
- **Base Matching**: Match patterns against the basename of the files.
- **Follow Symbolic Links**: Option to follow symbolic links in the search.

## Conclusion

This project provides a straightforward means of listing files via pattern matching, offering flexibility with several configuration options. It serves as a convenient tool for developers needing to work with file lists in Node.js environments, providing functionality akin to that of the 'glob' library.