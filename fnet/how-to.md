# Developer Guide for @fnet/list-files

## Overview

The `@fnet/list-files` library is a simple tool designed to help developers list files in a specified directory matching given patterns. It leverages the familiar glob pattern syntax to allow users to flexibly filter the files they are interested in. The primary function exported by this library, `index`, lets you specify patterns and options to customize the file listing process, making it easy to integrate into file management tasks.

## Installation

To use the `@fnet/list-files` library in your project, install it via npm or yarn:

```bash
npm install @fnet/list-files
```

or

```bash
yarn add @fnet/list-files
```

## Usage

### Basic Example

Start by importing the `index` function from the `@fnet/list-files` library. You can use this function to list files in a directory based on your specified pattern and options.

```javascript
import listFiles from '@fnet/list-files';

const files = listFiles({
  dir: './src',      // Directory to search in
  pattern: '*.js',   // Pattern to match JavaScript files
});

console.log(files); // Outputs list of JavaScript files in the './src' directory
```

### Customizing Options

You can further customize the search by using additional options like `nodir`, `dot`, `ignore`, `absolute`, `matchBase`, and `follow`. Each of these options influences how files are listed:

- `nodir`: When set to `true`, directories will not be included in the results.
- `dot`: Include files starting with a dot (e.g., `.gitignore`). Defaults to `true`.
- `ignore`: Patterns to exclude from the results.
- `absolute`: Outputs absolute paths if set to `true`.
- `matchBase`: When `true`, matches a pattern to a file path's basename.
- `follow`: Follows symbolic links if set to `true`.

### Advanced Example

```javascript
import listFiles from '@fnet/list-files';

// Search for all files except ignored ones, include dotfiles, and provide absolute paths
const files = listFiles({
  dir: './src',        // Directory to search in
  pattern: '**/*.js',  // Pattern to match all JavaScript files recursively
  ignore: 'node_modules/**',  // Ignore files in the node_modules directory
  dot: true,           // Include dotfiles
  absolute: true,      // Return absolute file paths
});

console.log(files); // Outputs absolute paths of all matching JavaScript files
```

## Examples

### Listing All Files in a Directory

```javascript
import listFiles from '@fnet/list-files';

const allFiles = listFiles({
  dir: '/my-project',
  pattern: '**/*', // Match all files and subdirectories
});

console.log(allFiles);
```

### Ignoring Specific File Types

```javascript
import listFiles from '@fnet/list-files';

const filesWithoutImages = listFiles({
  dir: '/my-project',
  pattern: '**/*',
  ignore: ['**/*.png', '**/*.jpg'], // Ignore image files
});

console.log(filesWithoutImages);
```

## Acknowledgement

The `@fnet/list-files` library uses `glob`, a well-known pattern-matching library, to perform its file search operations, allowing it to leverage powerful and flexible pattern matching capabilities.